<?php
// Site
$_['site_base']         = HTTP_SERVER;
$_['site_ssl']          = HTTP_SERVER;

// Language
$_['language_default']  = 'pt-pt,en-gb';
$_['language_autoload'] = array('pt-pt,en-gb');
$_['vendor_autoload']   = true;

// Database
$_['db_autostart']       = true;
$_['db_engine']          = DB_DRIVER; // mpdo, mysqli or pgsql
$_['db_hostname']        = DB_HOSTNAME;
$_['db_username']        = DB_USERNAME;
$_['db_password']        = DB_PASSWORD;
$_['db_database']        = DB_DATABASE;
$_['db_port']            = DB_PORT;

// Session
$_['session_autostart']  = false;
$_['session_engine']     = 'db';
$_['session_name']       = 'OCSESSID';

// Session
$_['session_engine']     = 'file';
$_['session_autostart']  = true;
$_['session_name']       = 'OCSESSID';

// Template
$_['template_engine']   = 'twig';
$_['template_cache']    = true;

// Actions
$_['action_default']    = 'common/home';
$_['action_router']     = 'startup/router';
$_['action_error']      = 'error/not_found';
$_['action_pre_action'] = array(
//	'startup/session',
	'startup/startup',
	'startup/language',
	'startup/database'
);

// Action Events
$_['action_event'] = array(
    'view/*/before' => array(
		'event/theme'
	)
);
