<?php
class ControllerCommonHome extends Controller {
	public function index() {
                $data = array();

		$this->load->language('common/home');
                // load models that will be used on
                // this controller and subsequent
		$this->load->model('common/home');
		$this->load->model('common/information');

		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
			$this->response->redirect($this->url->link('common/home'));
		}

                $data = array_merge($data, $this->model_common_home->get_data());

		$this->document->setTitle($data['title']);
		$this->document->setDescription($data['description']);
		$this->document->setKeywords($data['keywords']);

                // to later enable and disable sections
                $data['header']         = "";
                $data['top']            = "";
                $data['banner']         = "";
                $data['section_one']    = "";
                $data['section_two']    = "";
                $data['section_three']  = "";
                $data['section_four']   = "";
                $data['section_five']   = "";
                $data['section_footer'] = "";

		$data['base'] = HTTP_SERVER;

                // load sections
                $data['header']         = $this->load->controller('common/header');
                $data['top']            = $this->load->controller('common/top');
                $data['banner']         = $this->load->controller('common/banner');
                $data['section_one']    = $this->load->controller('common/section_one');
                $data['section_two']    = $this->load->controller('common/section_two');
                $data['section_three']  = $this->load->controller('common/section_three');
                $data['section_four']   = $this->load->controller('common/section_four');
                $data['section_five']   = $this->load->controller('common/section_five');
                $data['footer']         = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
