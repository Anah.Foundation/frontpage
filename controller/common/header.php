<?php
class ControllerCommonHeader extends Controller {
    public function index() {
        $this->load->language('common/header');

        $data['title'] = $this->document->getTitle();
        $data['heading_title'] = $this->document->getTitle();
        $data['description'] = $this->document->getDescription();
        $data['author'] = $this->language->get('author');

        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['scripts'] = $this->document->getScripts();

        $data['base'] = HTTP_SERVER;

        return $this->load->view('common/header', $data);
    }
}
