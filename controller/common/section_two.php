<?php
class ControllerCommonSectiontwo extends Controller {
    public function index() {
        $this->load->language('common/section_two');
        $data = array();

        $content_id = $this->model_common_home->get_section_two();

        $data = $this->model_common_information->get_information($content_id);

        $data['description'] = html_entity_decode($data['description']);

        $data['section_two_link'] = $this->model_common_home->get_section_two_link();

        $data['call_to_affiliate']      = $this->language->get('call_to_affiliate');
        $data['button_continue']              = $this->language->get('button_continue');

        return $this->load->view('common/section_two', $data);
    }
}
