<?php
class ControllerCommonSectionone extends Controller {

    public function index() {
        $this->load->language('common/section_one');

        $data=array();

        $content_id = $this->model_common_home->get_section_one();

        $data = $this->model_common_information->get_information($content_id);

        $dom = new DOMdocument();
        $dom->loadHTML(html_entity_decode($data['description']));

        $data['description'] = array();
        foreach($dom->getElementsByTagName('p') as $p){
            array_push($data['description'], $p->nodeValue);
        }

        $data['section_one_link'] = $this->model_common_home->get_section_one_link();

        $data['call_to_gift']   = $this->language->get('call_to_gift');
        $data['button_continue']      = $this->language->get('button_continue');

        return $this->load->view('common/section_one', $data);
    }
}
