<?php
class ControllerCommonSectionthree extends Controller {
    public function index() {
        $this->load->language('common/section_three');
        $data = array();

        $content_id = $this->model_common_home->get_section_three();

        $data = $this->model_common_information->get_information($content_id);

        $data['description'] = html_entity_decode($data['description']);

        $data['section_three_link'] = $this->model_common_home->get_section_three_link();

        $data['call_to_partner']      = $this->language->get('call_to_partner');
        $data['button_continue']            = $this->language->get('button_continue');

        return $this->load->view('common/section_three', $data);
    }
}
