<?php
class ControllerCommonFooter extends Controller {
    public function index() {
        $data = array();

        $this->load->language('common/footer');

        $data = array_merge($data, $this->model_common_home->get_data());

        $data['text_policy']   = $this->language->get('text_policy');
        $data['text_privacy']   = $this->language->get('text_privacy');
        $data['text_project'] = $this->language->get('text_project');
        $data['text_documentation'] = $this->language->get('text_documentation');
        $data['text_support'] = $this->language->get('text_support');

        $data['policy_link'] = "#";
        $data['privacy_link'] = "#";

        $data['version'] = VERSION;


        return $this->load->view('common/footer', $data);
    }
}
