<?php
class ControllerCommonBanner extends Controller {
    public function index() {
        $this->load->language('common/banner');

        $content_id = $data['banners'] = $this->model_common_home->get_banner();

        $data = $this->model_common_information->get_information($content_id);

        $data['description'] = html_entity_decode($data['description']);

        $data['button_continue']      = $this->language->get('button_continue');

        return $this->load->view('common/banner', $data);
    }
}
