<?php
class ControllerCommonTop extends Controller {
	public function index() {
		$this->load->language('common/top');
		$this->load->model('common/home');

                $data['title']          = $this->model_common_home->get_title();
                $data['text_home']      = $this->language->get('text_home');
                $data['home_link']      = HTTPS_SERVER;
                $data['text_shop']      = $this->language->get('text_shop');
                $data['top_link']       = $this->model_common_home->get_top_link();
                $data['call_to_action'] = $this->language->get('call_to_action');
                $data['call_to_action_link'] = $this->model_common_home->get_call_to_action_link();

		return $this->load->view('common/top', $data);
	}
}
