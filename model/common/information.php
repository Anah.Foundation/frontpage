<?php
class ModelCommonInformation extends Model {

    protected $pdo_info  =NULL;
    protected $pdo_infos = NULL;

    public function __construct($registry) {
        parent::__construct($registry);

        $this->pdo_info = $this->db->prepare("SELECT DISTINCT * FROM " . DB_PREFIX . "information i
            LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id)
            LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id)
                WHERE i.information_id = :information_id
                AND id.language_id = :language_id
                AND i2s.store_id = :store_id
                AND i.status = '1'");

        $this->pdo_infos = $this->db->prepare("SELECT * FROM " . DB_PREFIX . "information i
            LEFT JOIN " . DB_PREFIX . "information_description id ON (i.information_id = id.information_id)
            LEFT JOIN " . DB_PREFIX . "information_to_store i2s ON (i.information_id = i2s.information_id)
                WHERE id.language_id = :language_id
                AND i2s.store_id = :store_id
                AND i.status = '1'
                ORDER BY i.sort_order, LCASE(id.title) ASC");
    }

    public function get_information($info_id){

        $this->pdo_info->execute(array(
            'information_id'    => $info_id,
            'language_id'       => $this->config->get('config_language_id'),
            'store_id'          => $this->config->get('config_store_id')
        ));

        return $this->pdo_info->fetch();

    }
}
