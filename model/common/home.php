<?php
class ModelCommonHome extends Model {

    protected $data;

    public function __construct($registry){
        parent::__construct($registry);

        $data['title']          = $this->config->get('config_name');
        $data['description']    = $this->config->get('config_meta_description');
        $data['keywords']       = $this->config->get('config_meta_keywords');
        $data['telephone']      = $this->config->get('config_telephone');

        $data['conctact']  = $this->config->get('config_telephone');
        $data['facebook']  = "https://www.facebook.com/profile.php?id=100076239210612";
        $data['github']    = "https://codeberg.org/Anah.Foundation";
        $data['top_link']  = "https://social.anah.foundation/";
        $data['call_to_action_link']  = "https://shop.anah.foundation/register";

        // information id's
        // top, slogan / pitch
        $data['banner'] = 10;

        // call to action gift/voucher
        $data['section_one']            = 8;
        $data['section_one_link']       = "https://shop.anah.foundation/gift";

        // call to action affiliate
        $data['section_two']            = 9;
        $data['section_two_link']       = "https://shop.anah.foundation/affiliate";

        // call to action open shop/partner
        $data['section_three']  = 7;
        $data['section_three_link']       = "https://shop.anah.foundation/contact";

        $this->data = $data;
    }

    public function get_data(){
        return $this->data;
    }
    public function get_title(){
        return $this->data['title'];
    }
    public function get_description(){
        return $this->data['description'];
    }
    public function get_keywords(){
        return $this->data['keywords'];
    }
    public function get_telephone(){
        return $this->data['telephone'];
    }
    public function get_github(){
        return $this->data['github'];
    }
    public function get_facebook(){
        return $this->data['facebook'];
    }
    public function get_twitter(){
        return $this->data['twitter'];
    }
    public function get_linkdin(){
        return $this->data['linkdin'];
    }
    public function get_instagram(){
        return $this->data['instagram'];
    }
    public function get_top_link(){
        return $this->data['top_link'];
    }
    public function get_call_to_action_link(){
        return $this->data['call_to_action_link'];
    }
    public function get_banner(){
        return $this->data['banner'];
    }
    public function get_section_one(){
        return $this->data['section_one'];
    }
    public function get_section_one_link(){
        return $this->data['section_one_link'];
    }
    public function get_section_two(){
        return $this->data['section_two'];
    }
    public function get_section_two_link(){
        return $this->data['section_two_link'];
    }
    public function get_section_three(){
        return $this->data['section_three'];
    }
    public function get_section_three_link(){
        return $this->data['section_three_link'];
    }
}
