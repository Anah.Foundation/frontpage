<?php
// Text
$_['text_call_us']      = 'Give us a call';
$_['text_contact']      = 'Contact';
$_['text_home']         = 'Home';
$_['text_about_us']     = "About Us";
$_['text_services']     = 'Services';
$_['text_featured']     = 'Featured';
$_['text_view_all']     = 'View All';
$_['text_welcome']      = 'Welcome';
$_['text_search']       = 'Search';
$_['text_read_more']    = 'READ MORE';
$_['text_read_less']    = 'READ LESS';
$_['text_subscribe']    = 'SUBSCRIBE';
$_['text_newsletter']   = 'NEWSLETTER';
$_['text_shop']         = 'Shop';
$_['call_to_action']    = 'Sign Up';
$_['call_to_gift']      = 'Gift Offer';
$_['call_to_affiliate'] = 'Affiliate';
$_['call_to_partner']   = 'Open Shop';
$_['call_to_subscribe'] = 'RECEIVE OUR NEWSLETTER';
