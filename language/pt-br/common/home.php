<?php
// Text
$_['text_call_us']      = 'Contacte-nos';
$_['text_contact']      = 'Contacto';
$_['text_home']         = 'Home';
$_['text_about_us']     = "Quem somos";
$_['text_services']     = 'Serviços';
$_['text_featured']     = 'Destaques';
$_['text_view_all']     = 'Ver mais...';
$_['text_welcome']      = 'Bem Vindo';
$_['text_search']       = 'Procurar';
$_['text_read_more']    = 'CONTINUAR A LER';
$_['text_read_less']    = 'LER MENOS';
$_['text_subscribe']    = 'Subscrever';
$_['text_newsletter']   = 'Boletim de Notícias';
$_['text_shop']         = 'Loja';
$_['call_to_action']    = 'Registe-se';
$_['call_to_gift']      = 'Oferecer Prenda';
$_['call_to_affiliate'] = 'Afiliado';
$_['call_to_partner']   = 'Abrir Loja';
$_['call_to_subscribe'] = 'Subscreva Boletim de Notícias';
