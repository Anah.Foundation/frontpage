<section id="one" class="spotlight style1 bottom">
    <span class="image fit main"><img src="view/images/pic02.jpg" alt="" /></span>
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-4 col-12-medium">
                    <header>
                        <h2><?php echo $title; ?></h2>
                        <p><?php echo $meta_description; ?></p>
                    </header>
                </div>
                <div class="col-4 col-12-medium">
                    <p><?php echo $description[0]; ?></p>
                </div>
                <div class="col-4 col-12-medium">
                    <p><?php echo $description[1]; ?></p>
                </div>
            </div>
            <ul class="actions">
                <li><a href="<?php echo $section_one_link; ?>" class="button"><?php echo $call_to_gift; ?></a></li>
                <ul>
        </div>
    </div>
    <a href="#two" class="goto-next scrolly"><?php echo $text_next; ?></a>
</section>
