<section id="three" class="spotlight style3 left">
    <span class="image fit main bottom"><img src="view/images/pic04.jpg" alt="" /></span>
    <div class="content">
        <header>
            <h2><?php echo $title; ?></h2>
            <p><?php echo $meta_description; ?></p>
        </header>
        <?php echo $description; ?>
        <ul class="actions">
            <li><a href="<?php echo $section_three_link; ?>" class="button"><?php echo $call_to_partner; ?></a></li>
        </ul>
    </div>
    <a href="#four" class="goto-next scrolly"><?php echo $text_next; ?></a>
</section>
