<!DOCTYPE HTML>
<html>
    <head>
        <?php echo $header; ?>
    </head>
    <body class="is-preload landing">
        <div id="page-wrapper">

            <!-- Header -->
            <?php echo $top; ?>

            <!-- Banner -->
            <?php echo $banner; ?>

            <!-- One -->
            <?php echo $section_one; ?>

            <!-- Two -->
            <?php echo $section_two; ?>

            <!-- Three -->
            <?php echo $section_three; ?>

            <!-- Four -->
            <?php echo $section_four; ?>

            <!-- Five -->
            <?php echo $section_five; ?>

            <!-- Footer -->
            <?php echo $footer; ?>
        </div>

        <!-- Scripts -->
        <script src="view/js/jquery.min.js"></script>
        <script src="view/js/jquery.scrolly.min.js"></script>
        <script src="view/js/jquery.dropotron.min.js"></script>
        <script src="view/js/jquery.scrollex.min.js"></script>
        <script src="view/js/browser.min.js"></script>
        <script src="view/js/breakpoints.min.js"></script>
        <script src="view/js/util.js"></script>
        <script src="view/js/main.js"></script>

    </body>
</html>
