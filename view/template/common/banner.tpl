<section id="banner">
    <div class="content">
        <header>
            <h2><?php echo $title; ?></h2>
            <?php echo $description; ?>
        </header>
        <span class="image"><img src="view/images/pic01.jpg" alt="" /></span>
    </div>
    <a href="#one" class="goto-next scrolly"><?php echo $text_next; ?></a>
</section>


