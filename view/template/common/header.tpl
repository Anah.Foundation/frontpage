<title><?php echo $heading_title; ?></title>
<meta charset="utf-8" />
<meta name="author" content="<?php echo $author; ?>">
<meta name="description" content="<?php echo $description; ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
<link rel="stylesheet" href="view/css/main.css" />
<noscript><link rel="stylesheet" href="view/css/noscript.css" /></noscript>
