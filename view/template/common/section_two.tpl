<section id="two" class="spotlight style2 right">
    <span class="image fit main"><img src="view/images/pic03.jpg" alt="" /></span>
    <div class="content">
        <header>
            <h2><?php echo $title; ?></h2>
            <p><?php echo $meta_description; ?></p>
        </header>
        <?php echo $description; ?>
        <ul class="actions">
            <li><a href="<?php echo $section_two_link; ?>" class="button"><?php echo $call_to_affiliate; ?></a></li>
        </ul>
    </div>
    <a href="#three" class="goto-next scrolly"><?php $text_next;?></a>
</section>


