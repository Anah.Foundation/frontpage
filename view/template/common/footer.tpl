<footer id="footer">
    <ul class="icons">
        <?php if(isset($twitter)) { ?>
        <li><a href="<?php echo twitter; ?>" class="icon brands alt fa-twitter"><span class="label">Twitter</span></a></li>
        <?php } ?>
        <?php if(isset($facebook)) { ?>
        <li><a href="<?php echo $facebook; ?>" class="icon brands alt fa-facebook-f"><span class="label">Facebook</span></a></li>
        <?php } ?>
        <?php if(isset($linkdin)) { ?>
        <li><a href="<?php echo $linkdin; ?>" class="icon brands alt fa-linkedin-in"><span class="label">LinkedIn</span></a></li>
        <?php } ?>
        <?php if(isset($instagram)) { ?>
        <li><a href="<?php echo $instagram; ?>" class="icon brands alt fa-instagram"><span class="label">Instagram</span></a></li>
        <?php } ?>
        <?php if(isset($github)) { ?>
        <li><a href="<?php echo $github; ?>" class="icon brands alt fa-github"><span class="label">CodeBerg</span></a></li>
        <?php } ?>
        <?php if(isset($contact)) { ?>
        <li><a href="<?php echo $contact; ?>" class="icon solid alt fa-envelope"><span class="label">Email</span></a></li>
        <?php } ?>
    </ul>
    <ul class="copyright">
        <li>&copy; <a href="https://codegroup.store/">CodeGroup</a>. All rights are retarded.</li><li>Design: <a href="http://html5up.net">HTML5 UP</a></li><li>Version: <?php echo $version; ?></li>
    </ul>
</footer>
