<!-- Header -->
<header id="header">
    <h1 id="logo"><a href="<?php echo $top_link; ?>"><?php echo $title; ?></a></h1>
    <nav id="nav">
        <ul>
            <li><a href="<?php echo $home_link; ?>"><?php echo $text_home; ?></a></li>
            <li>
                <a href="#">Anah Foundation</a>
                <ul>
                    <li><a href="left-sidebar.html">Who</a></li>
                    <li><a href="right-sidebar.html">Why</a></li>
                    <li><a href="no-sidebar.html">How</a></li>
                    <li>
                        <a href="#">Spaces</a>
                        <ul>
                            <li><a href="https://social.anah.foundation/s/news/">News</a></li>
                            <li><a href="https://social.anah.foundation/s/welcome-space/">Welcome Space</a></li>
                            <li><a href="#">Option 3</a></li>
                            <li><a href="#">Option 4</a></li>
                        </ul>
                    </li>
                </ul>
            </li>
            <li><a href="https://shop.anah.foundation"><?php echo $text_shop; ?></a></li>
            <li><a href="<?php echo $call_to_action_link; ?>" class="button primary"><?php echo $call_to_action; ?></a></li>
        </ul>
    </nav>
</header>
