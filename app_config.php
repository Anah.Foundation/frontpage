<?php
// base directory where norte directory is
$directory = '/../';
// path or name of norte directory
$norte_path = '/shop.anah.foundation/';

// Check if SSL
if ((isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1'))) || $_SERVER['SERVER_PORT'] == 443) {
	$protocol = 'https://';
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https' || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == 'on') {
	$protocol = 'https://';
} else {
	$protocol = 'http://';
}

define('HTTP_SERVER', $protocol . $_SERVER['HTTP_HOST'] . rtrim(dirname($_SERVER['SCRIPT_NAME']), '/.\\') . '/');
define('HTTPS_SERVER', HTTP_SERVER);

// DIR
$root_dir = str_replace('\\', '/', realpath(dirname(__FILE__) . $directory) . $norte_path);
if(file_exists($root_dir . "system/startup.php")){
    define('DIR_SYSTEM', $root_dir."system/");
}else{
    define('DIR_SYSTEM', $root_dir . "upload/system/");
}
define('DIR_APPLICATION', str_replace('\\', '/', realpath(dirname(__FILE__))) . '/');
define('DIR_IMAGE', str_replace('\\', '/', realpath(dirname(__FILE__) . $directory)) . $norte_path .'image/');

unset($protocol);
unset($root_dir);
unset($norte_path);
unset($directory);
